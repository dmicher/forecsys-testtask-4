﻿using System;
using System.IO;

namespace DataGenerator
{
    class Program
    {
        static void Main()
        {
            FileInfo fi = new FileInfo("data.txt");
            Console.Write("Enter strings count for generate: ");
            int count = int.Parse(Console.ReadLine());
            Random rnd = new Random(Environment.TickCount);

            using (StreamWriter sw = fi.CreateText())
            {
                for (int i = 0; i < count; ++i)
                {
                    sw.WriteLine(
                        rnd.Next(0, (count - count / 4)) + ";" +
                        rnd.Next(1, 16) + ";" +
                        (CountryName)rnd.Next(0, 170));
                }
            }
        }
    }

    enum CountryName : int
    {
        Australia,
        Austria,
        Azerbaijan,
        AlandIslands,
        Albania,
        Algeria,
        Anguilla,
        Angola,
        Andorra,
        Argentina,
        Armenia,
        Aruba,
        Afghanistan,
        Bahamas,
        Bangladesh,
        Barbados,
        Bahrain,
        Belarus,
        Belize,
        Belgium,
        Benin,
        Bulgaria,
        Bolivia,
        Bosnia_and_Herzegovina,
        Botswana,
        Brazil,
        BruneiDarussalam,
        Burundi,
        Bhutan,
        VaticanCity,
        UnitedKingdom,
        Hungary,
        Venezuela,
        Timor, East,
        VietNam,
        Gabon,
        Haiti,
        Gambia,
        Ghana,
        Guadeloupe,
        Guatemala,
        Guinea,
        Guinea_Bissau,
        Germany,
        Gibraltar,
        HongKong,
        Honduras,
        Grenada,
        Greenland,
        Greece,
        Georgia,
        Guam,
        Denmark,
        Dominica,
        DominicanRepublic,
        Egypt,
        Zambia,
        WesternSahara,
        Zimbabwe,
        Israel,
        India,
        Indonesia,
        Jordan,
        Iraq,
        Iran,
        Ireland,
        Iceland,
        Spain,
        Italy,
        Yemen,
        Kazakhstan,
        Cambodia,
        Cameroon,
        Canada,
        Qatar,
        Kenya,
        Cyprus,
        Kyrgyzstan,
        Kiribati,
        China,
        Colombia,
        Korea,
        DPR,
        CostaRica,
        CotedIvoire,
        Cuba,
        Kuwait,
        LaoPDR,
        Latvia,
        Lesotho,
        Liberia,
        Lebanon,
        LibyanArabJamahiriya,
        Lithuania,
        Liechtenstein,
        Luxembourg,
        Mauritius,
        Mauritania,
        Madagascar,
        Macedonia,
        Malawi,
        Malaysia,
        Mali,
        Maldives,
        Malta,
        Morocco,
        Mexico,
        Mozambique,
        Moldova,
        Monaco,
        Mongolia,
        Namibia,
        Nepal,
        Niger,
        Nigeria,
        Netherlands,
        Nicaragua,
        NewZealand,
        Norway,
        UnitedArabEmirates,
        Oman,
        Pakistan,
        Panama,
        Paraguay,
        Peru,
        Poland,
        Portugal,
        Russia,
        Romania,
        SanMarino,
        SaudiArabia,
        Senegal,
        Serbia,
        Singapore,
        SyrianArabRepublic,
        Slovakia,
        Slovenia,
        Somalia,
        Sudan,
        USA,
        Tajikistan,
        Thailand,
        Tanzania,
        Togo,
        Tunisia,
        Turkmenistan,
        Turkey,
        Uganda,
        Uzbekistan,
        Ukraine,
        Uruguay,
        Micronesia,
        Fiji,
        Philippines,
        Finland,
        France,
        Croatia,
        Chad,
        Montenegro,
        CzechRepublic,
        Chile,
        Switzerland,
        Sweden,
        SriLanka,
        Ecuador,
        Eritrea,
        Estonia,
        Ethiopia,
        SouthAfrica,
        Jamaica,
        Japan,
    }
}

