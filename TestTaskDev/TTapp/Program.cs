﻿using System;
using System.IO;

namespace ForecsysTestTask
{
    /// <summary>
    /// Программа реализует требования тестового задания Forecsys при отклике на hh.ru
    /// https://hh.ru/vacancy/31342507
    /// (Бонус) Программа сверх приведённого задания перед выводом статистики сортирует данные по названию страны
    /// Для отключения бонусного функционала установить в флаге sortFlag значение false
    /// На 100 000 строк данных программа выполняется за примерно 30 секунд. Под б'ольшие объёмы она не оптимизирована.
    /// Разработчик: Черкасов Д.С.
    /// 28.05.2019
    /// </summary>
    class Program
    {
        /// <summary>
        /// Флаг для включения или выключения сортировки (только через код)
        /// </summary>
        private static readonly bool sortFlag = true;
        /// <summary>
        /// Карта мира: перечисление стран, учитываемых в текущем экземпляре программы
        /// </summary>
        private static WorldMap World;
        /// <summary>
        /// Предыдущая отсечка времени
        /// </summary>
        private static DateTime prevTime;
        /// <summary>
        /// Время запуска программы
        /// </summary>
        private static DateTime startTime;

        /// <summary>
        /// Точка входа в приложение. Метод управляет ходом его исполнения.
        /// </summary>
        static void Main()
        {
            // Делаем отсечки времени
            prevTime = startTime = DateTime.Now;
            // Читаем файл в массив строк данных. В случае неудачи закрываем программу
            if (!ReadFile("data.txt", out string[] txt))
            {
                Console.WriteLine("Ошибка чтения файла. Программа будет закрыта.");
                Console.ReadLine();
                return;
            }
            Console.Write($"Прошло {(DateTime.Now - prevTime).TotalSeconds} (сек.): Файл данных успешно прочитан. Всего прочитанных строк: {txt.Length}. Начата обработка данных." +
                              $"\nНа это может потребоваться некоторое время: ");
            prevTime = DateTime.Now;
            // Превращаем полученные из файла данные в объекты программы
            World = new WorldMap(txt.Length); // количество стран, очевидно, не больше количества строк файла
            for (int i = 0; i < txt.Length; ++i)
            {
                // Индикатор того, что программа работает
                if ((i % (txt.Length / 65)) == 0) Console.Write(".");
                // Метод ImportCountry обучен брасаться конкретным видом исключения, при появлении которого
                // дальнейшая работа по заполнению Карты мира бессмысленна - ловим, прерываем
                try
                {
                    ImportCountry(txt[i]);
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Source.GetType().ToString() + ":" + e.Message);
                    break;
                }
            }
            Console.Write($"\nПрошло {(DateTime.Now - prevTime).TotalSeconds} (сек.): Обработка данных завершена. Всего обнаружено стран: {World.Count}.\n");
            if (sortFlag) Console.WriteLine("Начата сортировка массива и вывод данных.\n");
            else Console.WriteLine("Начат вывод данных.\n");
            prevTime = DateTime.Now;
            // Выводим статистику на экран
            World.PrintStats(sortFlag);
            Console.WriteLine($"\n\nПрошло {(DateTime.Now - prevTime).TotalSeconds} (сек.): Вывод данных завершён.\n\nПрограмма для {txt.Length} строк данных выполнена за {(DateTime.Now - startTime)}");
            // Ждём реакцию пользователя перед закрытием программы
            Console.ReadLine();
        }

        #region Дополнительные методы программы
        /// <summary>
        /// Считывает данные из файла и передаёт их одной строкой в исходящий параметр.
        /// </summary>
        /// <param name="path">Путь к файлу, который нужно прочесть</param>
        /// <param name="txt">Результаты выполнения метода в виде массива, прочитанных из файла</param>
        /// <returns>Правда - чтение удалось, Ложь - чтение не удалось.</returns>
        private static bool ReadFile(string path, out string[] txt)
        {
            // смотрим информацию по файлу
            FileInfo fi = new FileInfo(path);
            // если файл не существует, его чтение невозможно
            if (!fi.Exists)
            {
                txt = null;
                return false;
            }
            // полностью считываем содержание файла и кратко проверяем его содержание
            using (StreamReader sr = new StreamReader(fi.OpenRead()))
                txt = sr.ReadToEnd().Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            // если файл был пустым, считаем, что его чтение не удалось
            if (txt == null || txt[0] == null || txt[0] == "")
            {
                txt = null;
                return false;
            }
            // если чтение удалось и содержание файла не нулевое, сообщаем об удачном исполнении чтенияы
            return true;
        }
        /// <summary>
        /// Проверяет валидность поступающих данных и добавляет валидные данные в качестве
        /// страны к карте мира. Невалидные данные игнорирует.
        /// </summary>
        /// <param name="str">Строка данных, соответствующая записи в формате: user_id;count;country</param>
        /// <exception cref="ArgumentException">Добавление невозможно</exception>
        private static void ImportCountry(string str)
        {
            // Целостность данных: налчие правильного количества разделителей и непустых подстрок с параметрами
            if (str == null || str == "") return;
            string[] parts = str.Split(';', StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 3) return;
            // Идентификатор пользователя: неотрицательное целое число
            if (!int.TryParse(parts[0], out int userId) || userId < 0) return;
            // Количество: неотрицательное целое число
            if (!int.TryParse(parts[1], out int count) || count < 0) return;
            // Дополнительная проверка названия страны не требуется

            // Метод Add класса WorlaMap обучен бросаться конкретными исключениями - отлавливаем и перебрасываем
            try
            {
                World.Add(new Country(parts[2], count, userId));
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw new ArgumentException("Текущее и дальнейшее добавление недоступно по причине: " + e.Message);
            }
        }
        #endregion
    }
    /// <summary>
    /// Объекты, обозначающие страну по данным этой программы
    /// </summary>
    class Country
    {
        #region Состояние страны
        /// <summary>
        /// Название страны
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Некоторое "количество" для этой страны
        /// </summary>
        public int Count { get; private set; }
        /// <summary>
        /// Перечисление уникальных идентификаторов пользователей для этой страны
        /// </summary>
        public int[] UserIDs { get; private set; }
        #endregion

        #region Конструкторы
        /// <summary>
        /// Конструктор создаёт объект "Страна" с заданными значениями.
        /// </summary>
        /// <param name="name">Название страны</param>
        /// <param name="count">Параметр "Количество"</param>
        /// <param name="userIds">Перечисление идентификаторов польозвателей</param>
        public Country(string name, int count, int[] userIds = null)
        {
            Name = name;
            Count = count;
            UnificateIDs(ref userIds);
            UserIDs = userIds;
        }
        /// <summary>
        /// Конструкто создаёт объект "Страна" с заданными значениями.
        /// </summary>
        /// <param name="name">Название страны</param>
        /// <param name="count">Параметр "Количетсво"</param>
        /// <param name="userId">Идентификатор пользователя</param>
        public Country(string name, int count, int userId) : this(name, count, new int[] { userId }) { }
        #endregion

        #region Основное поведение класса
        /// <summary>
        /// Складывает данные двух стран.
        /// Если их названия не равны, возвращает левый операнд без изменения.
        /// Если названия равны, то возвращает страну с их суммой по "количеству"
        /// и новым массивом пользователей, содержащим только уникальные значения
        /// </summary>
        /// <param name="A">Страна - левый операнд (к которой прибавляем)</param>
        /// <param name="B">Страна - правый операнд (которую прибавляем)</param>
        /// <returns>Новая страна с прибавленными значениями "Количество" и "Пользователи"</returns>
        public static Country operator +(Country A, Country B)
        {
            // Сложение двух стран с разными названиями не производится - возвращаяем левый операнд
            if (A.Name != B.Name) return A;
            // Складываем два массива с пользователями и удаляем из них неуникальных
            int ABLength = A.UserIDs.Length + B.UserIDs.Length;
            int[] sumArr = new int[ABLength];
            for (int i = 0; i < ABLength; ++i)
            {
                if (i < A.UserIDs.Length)
                    sumArr[i] = A.UserIDs[i];
                else
                    sumArr[i] = B.UserIDs[i - A.UserIDs.Length];
            }
            // Возвращаем новую страну с суммой "количества" и уникальных пользователей
            return new Country(A.Name, A.Count + B.Count, sumArr);
        }
        /// <summary>
        /// Из полученного массива идентификаторо пользователей удаляет неуникальные значения.
        /// </summary>
        /// <param name="userIDs">Массив пользователей, подлежащий унификации</param>
        private static void UnificateIDs(ref int[] userIDs)
        {
            // Если в массиве не больше 1 элемента, то все значения очевидно уникальны
            if (userIDs.Length < 2) return;
            // Узнаём количество неуникальных идентификаторов пользователей в массиве и помечаем (затираем) их значением "-1"
            int delCount = 0;
            for (int i = 0; i < userIDs.Length; ++i)
            {
                if (userIDs[i] < 0)
                {
                    ++delCount;
                    continue;
                }
                if (i < userIDs.Length)
                    for (int j = i + 1; j < userIDs.Length; ++j)
                        if (userIDs[j] == userIDs[i])
                            userIDs[j] = -1;
            }
            // Если все индексы уникальные, выходим - ничего менять не нужно
            if (delCount == 0) return;
            // Создаём массив нужного размера, состоящий только из уникальных идентификаторов пользователей
            int[] newArr = new int[userIDs.Length - delCount];
            delCount = 0;
            for (int i = 0; i < userIDs.Length; ++i)
            {
                if (userIDs[i] < 0)
                {
                    ++delCount;
                    continue;
                }
                newArr[i - delCount] = userIDs[i];
            }
            // Записываем ссылку на массив с уникальными идентификаторами пользователей в ссылку вызывающего кода
            userIDs = newArr;
        }
        #endregion

        #region Основное поведение объектов
        /// <summary>
        /// Выводит данные по указанной стране в формате задания:
        /// "country;sum(count) (сумма по count);count_uniq(user_id) (число уникальных user_id для country)"
        /// </summary>
        /// <returns>Форматированная строка данных вывода по условиям задания</returns>
        public override string ToString() => $"{Name};{Count};{UserIDs.Length}";
        #endregion
    }

    /// <summary>
    /// Объекты, обозначающие наборы стран мира и их поведение
    /// </summary>
    class WorldMap
    {
        #region Состояние
        /// <summary>
        /// Набор стран, учтённых на мировой карте
        /// </summary>
        public Country[] Countries { get; private set; }
        /// <summary>
        /// Возвращает количество стран, учтённых на карте мира
        /// </summary>
        public int Count
        {
            get
            {
                for (int i = 0; i < Length; ++i)
                    if (Countries[i] == null) return i;
                return Length;
            }
        }
        /// <summary>
        /// Возвращает максимальный размер карты мира по количеству стран
        /// </summary>
        private int Length { get => Countries.Length; }
        #endregion

        #region Конструкторы
        /// <summary>
        /// Создаёт карту указанного размера
        /// </summary>
        /// <param name="length">Размер карты в количестве стран</param>
        public WorldMap(int length) { Countries = new Country[length]; }
        #endregion

        #region Основное поведение объектов
        /// <summary>
        /// Добавляет объект "Страна" в имеющийся набор стран с учётом совпадающих стран
        /// </summary>
        /// <param name="country">Добавляемая страна</param>
        /// <exception cref="ArgumentOutOfRangeException">Размер массива карты мира оказался меньше количества стран</exception>
        public void Add(Country country)
        {
            for (int i = 0; i < Length; ++i)
            {
                // Если все учтённые страны закончились, в первой из них записываем новую страну
                if (Countries[i] == null)
                {
                    Countries[i] = country;
                    return;
                }
                // Если страна с таким именем уже была учтена на карте, то её прибавляем к имеющейся
                if (Countries[i].Name == country.Name)
                {
                    Countries[i] += country;
                    return;
                }
            }
            // если достигли этого кода, значит размер массива со странами, оказался меньше нужного - кидаем исключение
            throw new ArgumentOutOfRangeException(
                $"Не удалось добавить запись: {country.ToString()}. " +
                $"Превышен размер карты мира в {Length} стран.");
        }
        /// <summary>
        /// Выводит в консоль все страны, отсортированные по названию
        /// </summary>
        public void PrintStats(bool sortFlag)
        {
            if (sortFlag)
            {
                Country[] output = new Country[Count];
                for (int i = 0; i < output.Length; ++i)
                    output[i] = Countries[i];
                // Перед выводом информации сортируем страны по имени (бонусом)
                SortByName(output, 0, output.Length - 1);
                // Выводим информацию на экран
                foreach (Country country in output)
                    Console.WriteLine(country.ToString());
                return;
            }
            // Если сортировка не требуется
            foreach (Country country in Countries)
            {
                if (country == null) break;
                Console.WriteLine(country.ToString());
            }
        }
        #endregion

        #region Основное поведение класса
        /// <summary>
        /// Производит сортировку массива стран по их названию
        /// Сортировка реализуется через алгоритм с деление массива по Хоару
        /// </summary>
        private static void SortByName(Country[] arr, int first, int last)
        {
            // На каждом входе в рекурсию объявляем переменные
            // Берём название страны из середины массива (или его части) (целочисленное деление)
            string p = arr[(last - first) / 2 + first].Name;
            // устанавливаем "бегунки" на края массива (его части)
            int i = first, j = last;
            // Пока бегунки не встретились
            while (i <= j)
            {
                // сдвигаем бегунки, пока не обнаружим первые неостортированные значения или не пересечём центр массива
                while (arr[i].Name.CompareTo(p) < 0 && i <= last) ++i;
                while (arr[j].Name.CompareTo(p) > 0 && j >= first) --j;
                // Если бегунки пробежали мимо друг друга, перекидываем значения (единичничная операция сортировки)
                if (i <= j)
                {
                    Country tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                    ++i;
                    --j;
                }
            }
            // Если часть массива на текущем уровне рекурсии больше 1 элемента, разворачиваем рекурсию глубже
            if (j > first) SortByName(arr, first, j);
            if (i < last) SortByName(arr, i, last);
            // при достижении этого места выходим из текущего уровня рекурсии
        }
        #endregion
    }
}
